import numpy as np

epsilon=10**(-12)

######################################## FONCTION FAITES AVEC LOIC CONCERNANT LE TRI ####################################################################

def plusgrand(x,y): ################### retourne 1 si x>y et si x<=y retourne 0 (classement dans R2)
    if abs(x[0]-y[0])<epsilon and x[1]>y[1]+epsilon:
        i=1
    elif (x[0]>y[0]+epsilon):
        i=1
    else:
        i=0
    return(int(i))

def egal(x,y): ############################## retourne 1 si x=y, sinon 0
    if abs(x[0]-y[0])<epsilon and abs(x[1]-y[1])<epsilon:
        i=1
    else:
        i=0
    return(i)


def trirapide(U,idebut,ifin): ############## ne renvoie rien, mais modifie directement le vecteur U, après il est trié
    if ifin< idebut:
        truc=0
    elif ifin==idebut:
        truc=0
    else:
        ipivot=int((idebut+ifin)*0.5)
        pivot=np.zeros(3)
        pivot[0]=U[ipivot,0] ####### on a pas mis pivot[:]=U[ipivot,:] psq sinon ça l'enregistre pas (juste pointeur qqchose comme ça) --> partout pareil
        pivot[1]=U[ipivot,1]
        pivot[2]=U[ipivot,2] ############## il faut mettre des vecteurs à 3 colonnes (3ème utile pour la signature)
        i=idebut
        jdebut=idebut
        jfin=ifin
        for ii in range(idebut,ifin+1):
            if i != ipivot:
                if (plusgrand(pivot,U[i,:])==1):
                    U[jdebut,0]=U[i,0]
                    U[jdebut,1]=U[i,1]
                    U[jdebut,2]=U[i,2]
                    jdebut=jdebut+1
                    i=i+1
                else:
                    V=np.zeros(3)
                    V[0]=U[jfin,0]
                    V[1]=U[jfin,1]
                    V[2]=U[jfin,2]
                    U[jfin,0]=U[i,0]
                    U[jfin,1]=U[i,1]
                    U[jfin,2]=U[i,2]
                    U[i][0]=V[0]
                    U[i][1]=V[1]
                    U[i][2]=V[2]
                    if jfin==ipivot:
                        ipivot=i
                    jfin=jfin-1
            else:
                i=i+1
        U[jdebut,0]=pivot[0]
        U[jdebut,1]=pivot[1]
        U[jdebut,2]=pivot[2]
        trirapide(U,idebut,jdebut-1)
        trirapide(U,jdebut+1,ifin)
    return()

#trirapide(pts,0,len(pts)-1) on trira qd on a tout les pts ddl glob


def effa(U): ############# fonction qui efface les points répétés (donc il faut y mettre un vecteur trié dedans)
    l=0
    N=np.shape(U)[0]
    tri=np.zeros(N,dtype=int)
    for i in range(N-1):
        if egal(U[i,:],(U[i+1,:]))==1:
            l=l+1
    V=np.zeros((N-l,2))

    l2=0

    for i in range(N-1):
        if egal(U[i,:],(U[i+1,:]))==0:
            V[l2,0]=U[i,0]
            V[l2,1]=U[i,1]
            tri[i]=l2
            l2=l2+1
        else:
            tri[i]=l2

    if egal(U[-1,:],U[-2,:])==0:
        tri[-1]=l2-egal(U[N-2,:],U[N-1,:])
    else:
        tri[-1]=l2-egal(U[N-2,:],U[N-1,:])+1

    V[N-l-1,0]=U[N-1,0]
    V[N-l-1,1]=U[N-1,1]
    return(V,tri) ######### renvoie aussi le vecteur tri = la signature

