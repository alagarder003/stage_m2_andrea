import numpy as np
import random as rd
import scipy as sc
import scipy.sparse as sp
from matplotlib import pyplot as plt
from matplotlib import cm
import mpl_toolkits.mplot3d
import scipy.sparse.linalg
import math
import time
import cmath
import operator as o

import Maillage as m
import test_compression_green as comp
from codetri import *
from golub import *
from bibmaillage import *
from green import *
from representation import *
from bibmatrice import *
from Dirichlet import *


np.set_printoptions(threshold=np.inf) ########################## permet d'imprimer tout le vecteur


def resolution_dirichletex(mg,kappa,I,J,donnee):
    ################################## CRÉATION DU MAILLAGE ###################################
    x1,x2,x3,x4,h=mg.creation(I,J,donnee) ################## création du code (a donner a TRIANGLE si on veut le voir) pour avoir un maillage hyper régulier
    stilde=source(mg)
    n1,pts=crea_pts() 
    n2,ele=crea_ele()
    K,ddloc,nbGD,f,f2,lambda1=fonc_donnee(n1,n2) 
    kappa=mg.k0/mg.alpha
    loc2loc,loc1toloc2=crea_loc2loc(K) 
    I=K+1
    X,rho=Gauss(I) 
    T=crea_aire(n2,pts,ele)
    Airepara=aire_para(x1,x2,x3)
    #################################### CRÉATION DES DDL CONT ET NON ###########################
    XC=Xchapeau(K,loc2loc)
    ptsGD=crea_ptsGD(K,n2,pts,ele,XC,nbGD)
    #ptsGDtrié=1*ptsGD
    t00=time.time()
    #trirapide(ptsGDtrié,0,len(ptsGD)-1) #on tri qd on a tt les pts ddl glob donc c'est mtn les ddlglob de GD #codetri
    ptsGDtrié=np.reshape(sorted(ptsGD,key=o.itemgetter(0,1)),np.shape(ptsGD))
    glob2loc=crea_glob2loc(n2,K,nbGD)   
    ptsGCtrié,tri=effa(ptsGDtrié)
    t0=time.time()
    print('temps tri+eff',nbGD,t0-t00)
    nbGC=tri[nbGD-1]+1 
    print(nbGC)
    loc2glob=crea_loc2glob(nbGD,ptsGDtrié,tri,glob2loc,n2)
    ######################################### FACES DE BORD ####################################
    bas=0 
    gauche=1
    haut=2
    droit=3
    nbface=n2*3
    face=crea_face(n2,pts,ele,nbface)
    face=quelbord(face,nbface,mg)
    nbfacebord=calcul_nbfacebord(face,nbface)
    ddlfacebord=nbfacebord*(K+1) 
    facebord=crea_facebord(nbface,face,nbfacebord) 
    ptsGDfaceDisc=crea_ptsGDfaceDisc(K,nbfacebord,facebord,ddlfacebord,loc2glob)
    loc2globface=crea_loc2globface(K,nbfacebord,ptsGDfaceDisc)
    nbas,ngauche=normale(mg)
    ################################# MATRICES DE RÉFÉRENCE ####################################
    Mc=masse_chapeau(K,loc2loc,rho,X)
    Kcxx,Kcxy,Kcyx,Kcyy=rig_locales(K,loc2loc,rho,X)
    I2=2*(K+1)
    XI2,rhoI2=Gauss(I2)
    ###################################### SECOND MEMBRE #####################################
    F=nodal(nbGC,ptsGCtrié,f,stilde)
    ############################# CONSTRUCTION DES MATRICES ##################################
    tmat1=time.time()
    R=rig_globale(n2,K,pts,ele,T,Kcxx,Kcxy,Kcyx,Kcyy,loc2loc,loc2glob,nbGC)
    tmat2=time.time()
    print('temps rigidité',tmat2-tmat1)
    Masse=masse(n2,K,Mc,loc2loc,loc2glob,nbGC,T)
    tmat3=time.time()
    print('temps masse',tmat3-tmat2)
    ####################### RÉSOLUTION DU PROBLÈME ET TRACÉ DE LA SOLUTION ######################
    e=(R-kappa**2*Masse) ########### pb : -Delta p -k_0^2 p = f      cond : u=uex
    Ed=dirichlet(e,nbfacebord,loc2globface,nbGC,K)
    II=Id_dirichlet(ddlfacebord,nbfacebord,K,loc2globface,nbGC)
    MF=Masse*F
    MF2=Fdirichlet(MF,nbfacebord,loc2globface,K)
    print('creation Uexbord')
    t3=time.time()
    #Uexbord=def_Uexbord(stilde,ptsGDfaceDisc,ddlfacebord,kappa)
    t4=time.time()
    Uexbord2=def_Uexbord2(ptsGDfaceDisc,ddlfacebord,kappa,stilde)
    t5=time.time()
    #print('construction rpz',t4-t3)
    print('construction integ',t5-t4)
    #Ud=Ud_dirichlet(Uexbord,nbGC,nbfacebord,loc2globface,K)
    Ud2=Ud_dirichlet(Uexbord2,nbGC,nbfacebord,loc2globface,K)
    t1=time.time()
    #U=sp.linalg.spsolve(Ed+II,MF2+Ud)
    t2=time.time()
    U2=sp.linalg.spsolve(Ed+II,MF2+Ud2)
    t3=time.time()
    #print('temps resolution rpz dirichlet',t2-t1)
    print('temps inversion integ dirichlet',t3-t2)
    x = ptsGCtrié[:,0]
    y = ptsGCtrié[:,1]
    #plt.figure()
    #plt.scatter(np.arange(ddlfacebord),(Uexbord-Uexbord2).imag) 
    #plt.title("difference rpz integ")
    return(Masse,nbGC,x,y,U2)

def def_Uexbord(stilde,ptsGDfaceDisc,ddlfacebord,kappa):
    # Definition du maillage d'integration
    xfc=stilde[0]
    yfc=stilde[1]
    Lxf=1.83
    Lyf=1.83
    Nxf=67
    Nyf=67
    xf,yf=maillage2(xfc,yfc,Lxf,Lyf,Nxf,Nyf)
    gridxf,gridyf,gridf=source2(xf,yf)
    xrep2=ptsGDfaceDisc[:,0]
    yrep2=ptsGDfaceDisc[:,1]
    res2=np.zeros(ddlfacebord,dtype=complex)
    for i in range(ddlfacebord):
        res2[i]=representaion(kappa,xrep2[i],yrep2[i],gridxf,gridyf,gridf,xf,yf)
    return(res2)

def j0k(r,kappa):
    return(sc.special.j0(kappa*r)*f2(r)*r)

def h10k(r,kappa):
    return(sc.special.hankel1(0,kappa*r)*f2(r)*r)

def int1(rx,kappa):
    return(sc.integrate.quad(j0k,0,rx,args=kappa,complex_func=True)[0])

def vecint1(rxvec,kappa):
    fx1=np.zeros(len(rxvec),dtype=complex)
    for i in range(len(rxvec)):
        fx1[i]=sc.integrate.quad(j0k,0,rxvec[i],args=kappa,complex_func=True)[0]
    return(fx1)

def int2(rx,kappa):
    return(sc.integrate.quad(h10k,rx,np.inf,args=kappa,complex_func=True)[0])

def vecint2(rxvec,kappa):
    fx2=np.zeros(len(rxvec),dtype=complex)
    for i in range(len(rxvec)):
        fx2[i]=sc.integrate.quad(h10k,rxvec[i],np.inf,args=kappa,complex_func=True)[0]
    return(fx2)

def def_Uexbord2(ptsGDfaceDisc,ddlfacebord,kappa,stilde):
    Uexbord=np.zeros(ddlfacebord,dtype=complex)
    for i in range(ddlfacebord):
        rx=np.linalg.norm(ptsGDfaceDisc[i,0:2]-stilde)
        s1=sc.special.hankel1(0,kappa*rx)*int1(rx,kappa)
        s2=sc.special.j0(rx*kappa)*int2(rx,kappa)
        Uexbord[i]=(s1+s2)*np.pi*0.5j
    return(Uexbord)

def normex(points,stilde):
    r=np.zeros(np.shape(points)[0])
    for i in range(np.shape(points)[0]):
        r[i]=np.linalg.norm(points[i,0:2]-stilde)
    return(r)

def deraf(Nx,rmax,ordre,kappa):
    xx=np.zeros(Nx+ordre+3)
    fx1=np.zeros(Nx+ordre+3,dtype=complex)
    fx2=np.zeros(Nx+ordre+3,dtype=complex)
    hx=rmax/Nx
    for j in range(Nx+ordre+3):
        xx[j]=j*hx
        fx1[j]=sc.integrate.quad(j0k,0,xx[j],args=kappa,complex_func=True)[0]
        fx2[j]=sc.integrate.quad(h10k,xx[j],np.inf,args=kappa,complex_func=True)[0]
    return(xx,fx1,fx2)

def ma_fonction_interpolée(rho,ordre,x,fx):
    j=int(rho/(x[1]-x[0]))
    lambda0 = (rho-x[j])/(x[j+ordre]-x[j])
    fr=0
    for k in range(ordre+1):
        fr=fr+fx[j+k]*phi(lambda0,k,ordre)
    return fr

def solexacte_partout(mg,kappa,I,J,donnee):
    ################################# CRÉATION DU MAILLAGE ###################################
    x1,x2,x3,x4,h=mg.creation(I,J,donnee) ################## création du code (a donner a TRIANGLE si on veut le voir) pour avoir un maillage hyper régulier
    stilde=source(mg)
    n1,pts=crea_pts() 
    n2,ele=crea_ele()
    K,ddloc,nbGD,f,f2,lambda1=fonc_donnee(n1,n2) 
    kappa=mg.k0/mg.alpha
    loc2loc,loc1toloc2=crea_loc2loc(K) 
    I=K+1
    X,rho=Gauss(I) 
    T=crea_aire(n2,pts,ele)
    Airepara=aire_para(x1,x2,x3)
    #################################### CRÉATION DES DDL CONT ET NON ###########################
    XC=Xchapeau(K,loc2loc)
    temps=time.time()
    ptsGD=crea_ptsGD(K,n2,pts,ele,XC,nbGD)
    tempstemps=time.time()
    print('creation pts',tempstemps-temps)
    ptsGDtrié=1*ptsGD
    t00=time.time()
    #trirapide(ptsGDtrié,0,len(ptsGD)-1) #on tri qd on a tt les pts ddl glob donc c'est mtn les ddlglob de GD #codetri
    ptsGDtrié=np.reshape(sorted(ptsGD,key=o.itemgetter(0,1)),np.shape(ptsGD))
    tm=time.time()
    print('temps tri',nbGD,tm-t00)
    ptsGCtrié,tri=effa(ptsGDtrié)
    t0=time.time()
    print('temps eff',nbGD,t0-tm)
    glob2loc=crea_glob2loc(n2,K,nbGD)   
    nbGC=tri[nbGD-1]+1 
    Uexacte=np.zeros(nbGC,dtype=complex)
    rx=normex(ptsGCtrié,stilde)
    Nx=int(nbGC/100)
    rmax=np.max(rx)
    t=time.time()
    xx,fx1,fx2=deraf(Nx,rmax,K+1,kappa)
    fr1=np.zeros(nbGC,dtype=complex)
    fr2=np.zeros(nbGC,dtype=complex)
    for i in range(nbGC):
        rho=rx[i]
        fr1[i]=ma_fonction_interpolée(rho,K+1,xx,fx1)
        fr2[i]=ma_fonction_interpolée(rho,K+1,xx,fx2)
        Uexacte[i]=np.pi*0.5j*(sc.special.hankel1(0,rx[i]*kappa)*fr1[i]+sc.special.j0(rx[i]*kappa)*fr2[i])
    tt=time.time()
    print('temps construction uex',nbGC,tt-t)
    x = ptsGCtrié[:,0]
    y = ptsGCtrié[:,1]
    return(x,y,Uexacte)

def solexacte_simple(nbGC,stilde,ptsGCtrié,kappa):
    Uexacte=np.zeros(nbGC,dtype=complex)
    rx=normex(ptsGCtrié,stilde)
    Nx=int(nbGC/100)
    rmax=np.max(rx)
    t=time.time()
    xx,fx1,fx2=deraf(Nx,rmax,K+1,kappa)
    fr1=np.zeros(nbGC,dtype=complex)
    fr2=np.zeros(nbGC,dtype=complex)
    for i in range(nbGC):
        rho=rx[i]
        fr1[i]=ma_fonction_interpolée(rho,K+1,xx,fx1)
        fr2[i]=ma_fonction_interpolée(rho,K+1,xx,fx2)
        if rx[i]==0:
            Uexacte[i]=np.pi*0.5j*(sc.special.hankel1(0,10**(-10)*kappa)*fr1[i]+sc.special.j0(10**(-10)*kappa)*fr2[i])
        else:
            Uexacte[i]=np.pi*0.5j*(sc.special.hankel1(0,rx[i]*kappa)*fr1[i]+sc.special.j0(rx[i]*kappa)*fr2[i])
    tt=time.time()
    print('temps construction uex',nbGC,tt-t)
    return(Uexacte)

def enreg_solex(x,y,Uexacte):
    nbGC=len(Uexacte)
    t1=time.time()
    with open('solexacte.txt','w') as file:
        file.write(f"{nbGC} \n")
        for i in range(nbGC):
            if i%100==0:
                print(i)
            file.write(f"{x[i]} {y[i]} {Uexacte[i]} \n")
    t2=time.time()
    print('temsp enregist sol exacte partout', t2-t1)

def enreg_masse(Masse):
    t1=time.time()
    rowM,colM=sp.csc_matrix.nonzero(Masse)
    t2=time.time()
    print('temps récup indice masse',t2-t1)
    t1=time.time()
    with open('matmasse.txt','w') as file:
        file.write(f"{len(rowM)} \n")
        print(len(rowM))
        for i in range(len(rowM)):
            if i%1000==0:
                print(i)
            file.write(f"{rowM[i]} {colM[i]} {Masse.data[i]} \n")
    t2=time.time()
    print('temsp enregist mat masse', t2-t1)
