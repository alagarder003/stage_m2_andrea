import numpy as np
import matplotlib.pyplot as plt

######################################### ALGORITHME DE GOLUB FAIT AVEC LOIC (INITIALISATION DE LA QUADRATURE DE GAUSS) ##################################  
    

def Gauss(I): ##### I=ORDRE DE METH DE GAUSS
    Dgauss=np.zeros((I,I))
    Mgauss=np.zeros((I,I))
    for i in range(I):
        Dgauss[i,i]=2*(i+1)-1

    for i in range(I-1):
        Mgauss[i,i+1]=i+1
        Mgauss[i+1,i]=i+1

    X=np.linalg.eigvals(np.dot(Mgauss,np.linalg.inv(Dgauss)))
    Agauss=np.zeros((I,I))
    Fgauss=np.zeros((I,1))
    for i in range(I):
        for j in range(I):
            Agauss[i,j]=X[j]**i

    for i in range(0,I,2):
        Fgauss[i][0]=2/(i+1)
    
    rho=np.dot(np.linalg.inv(Agauss),Fgauss)
    rho=rho/2
    X=(X+1)/2
    return(X,rho)

def Gauss2D(I):
    X,rho=Gauss(I)
    Xg=np.zeros((I**2,2)) ########### point et poids de Gauss dans le triangle de référence
    rhog=np.zeros(I**2)
    l=0
    for i in range(I):
            for j in range(I):
                Xg[l,:]=(X[i],(1-X[i])*X[j])
                rhog[l]=rho[i]*rho[j]*(1-X[i])
                l=l+1
    return(Xg,rhog)


def crea_GaussEle(n2,I,pts,ele):
    XE=np.zeros((n2,I**2,2)) ########### point de Gauss dans chaque élément
    Xg,rhog=Gauss2D(I)
    for n in range(n2):
        A1=pts[ele[n,0],:]
        A2=pts[ele[n,1],:]
        A3=pts[ele[n,2],:]
        l=0
        for i in range(I):
            for j in range(I):
                XE[n,l,:]=Xg[l,0]*A1+Xg[l,1]*A2+(1-Xg[l,0]-Xg[l,1])*A3
                l=l+1
    return(XE)

def trace_GaussEle(Xg,XE,n):
    plt.figure()
    plt.scatter(Xg[:,0],Xg[:,1])
    plt.title('gauss ref')
    plt.figure()
    plt.scatter(XE[n,:,0],XE[n,:,1])
    plt.title('gauss ele')
