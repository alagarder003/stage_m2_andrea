import numpy as np
from math import floor

########## j'ai copié cette fonction ici pour créer le maillage triangle du parallélogramme
def x2xtilde(x,mg): ############## transformation de x à xtitlde en multpliant par la matrice B-1
    M=np.reshape(mg.M,(np.size(mg.M),1))
    Bm1=np.identity(np.size(mg.M))+np.dot(M,M.T)/(mg.alpha*(mg.alpha+1))
    xtilde=np.dot(Bm1,x)
    return(xtilde)


############# création du maillage
class maillage:
    def __init__(self):
        self.M = [0.7,0.5] # vecteur de norme<1
        self.alpha=np.sqrt(1-np.dot(self.M,self.M))
        self.k0 = 2*np.pi*self.alpha # je veux arriver à 2pi après transformation
        self.L = 18 # taille du domaine (carré [0,L]x[0,L])

    def triangle(self):
        ########## créer un fichier "mesh.poly" q'uil faudra donner a TRIAGLE pour créer le maillage du parallélogramme (pas très régulier en fait)
        o0=[0,0]
        o1=[self.L,0]
        o2=[self.L,self.L]
        o3=[0,self.L]
        ot0=x2xtilde(o0,self)
        ot1=x2xtilde(o1,self)
        ot2=x2xtilde(o2,self)
        ot3=x2xtilde(o3,self)       
        with open("mesh.poly","w") as file:
            file.write(f"## list of nodal points \n")
            file.write(f"#------------------------------------------------------------------------\n")
            file.write(f"           4       2       0       0\n")
            file.write(f"           1       {ot0[0]}        {ot0[1]}        0\n")
            file.write(f"           2       {ot1[0]}        {ot1[1]}        0\n")
            file.write(f"           3       {ot2[0]}        {ot2[1]}        0\n")
            file.write(f"           4       {ot3[0]}        {ot3[1]}        0\n")
            file.write(f"## list of segments \n")
            file.write(f"#------------------------------------------------------------------------\n")
            file.write(f"           4       2 \n")
            file.write(f"           1       1        2        0\n")
            file.write(f"           2       2        3        0\n")
            file.write(f"           3       3        4        0\n")
            file.write(f"           4       4        1        0\n")
            file.write(f"## hole and zone (0 for nothing) \n")
            file.write(f"#------------------------------------------------------------------------\n")
            file.write(f"           0 \n")
            file.write(f"           0 \n")

    def creation(self,I,J,donnee): ################## créer les fichiers .node et .ele pour une maillage hyper régulier (faire tourner 2 fois du coup)
        o0=[0,0] ########### coins du carré
        o1=[self.L,0]
        o2=[self.L,self.L]
        o3=[0,self.L]
        ot0=x2xtilde(o0,self) ############## coins du parallélogramme
        ot1=x2xtilde(o1,self)
        ot2=x2xtilde(o2,self)
        ot3=x2xtilde(o3,self)
        l1=np.linalg.norm(ot1-ot0) ############# longueur des cotés du parallélogramme
        l2=np.linalg.norm(ot3-ot0)
        I=int(I)
        J=int(J)
        h1=l1/I ############## longueur d'un triangle sur les bords hauts et bas
        h2=l2/J ######## pareil pour gauche et droite (normalement proche de h1)
        if donnee==1:
            print('longueur bas:',l1)
            print('longueur gauche:',l2)
            print('longueur trangle bas:',h1)
            print('longueur trangle gauche:',h2)
            print('N bas:',I)
            print('N gauche:', J)
        x=np.zeros(2)
        y=np.zeros(2)
        z=np.zeros(2)
        x=(ot1-ot0)/I
        y=(ot3-ot0)/J
        tab=np.zeros((I+1,J+1))
        l=0
        for i in range(I+1):
            for j in range(J+1):
                tab[i,j]=l+1
                l=l+1
        if donnee==1:
            print('nb point maillage:',l)
        with open("maillage_reg.node","w") as file: ############# création des points du maillage et du .node
            file.write(f"{l} 2 0 1 \n")
            l=1
            for i in range(I+1):
                for j in range(J+1):
                    z[0]=i*x[0]+j*y[0]
                    z[1]=i*x[1]+j*y[1]
                    file.write(f"{l} {z[0]} {z[1]} 1 \n")
                    l=l+1
        e=0
        for i in range(I):
            for j in range(J):
                e=e+1
                e=e+1
        if donnee==1:
            print('nb ele:',e)
        with open("maillage_reg.ele","w") as file: ############## création des éléments (on lie les points) et du .ele
            file.write(f"{e} 3 0 \n")
            e=1
            for i in range(I):
                for j in range(J):
                    lbg=tab[i,j]
                    lbd=tab[i+1,j]
                    lhg=tab[i,j+1]
                    lhd=tab[i+1,j+1]
                    file.write(f"{e} {lhd} {lbg} {lbd} \n")
                    e=e+1
                    file.write(f"{e} {lhd} {lhg} {lbg} \n")
                    e=e+1
        return(ot0,ot1,ot2,ot3,(h1+h2)/2)
            
