import numpy as np
import scipy.sparse as sp
import scipy as sc
from bibmaillage import K
from bibmaillage import ddloc
from bibmaillage import loc2globpetit
import matplotlib.pyplot as plt
from matplotlib import cm
from golub import *


########################### FONCTIONS FAITES AVEC LOIC CONCERNANT LA CONSTRUCTION DES MATRICES DE MASSE ET RIGIDITE ######################################


def phi(x,ilocf,K): ################ fonction de base 1D sur le segment [0,1]
    p0=1
    for i in range(K+1):
        if i!=ilocf:
            p0=p0*(ilocf/K-i/K)
    p=1
    for i in range(K+1):
        if i!=ilocf:
            p=p*(x-i/K)
    return(p/p0)


def fctbase(x,y,i,j,K): ############# fonction de base dans le triangle de référence
    s=1
    i3=K-i-j
    for t in range(i):
        s=s*(x-t/K)/(i/K-t/K)
    for t in range(j):
        s=s*(y-t/K)/(j/K-t/K)
    for t in range(i3):
        s=s*(1-y-x-t/K)/(i3/K-t/K)
    return(s)


def deriv_x_fctbase(x,y,i,j,K): ######### approximation de la dérivée des fonctions de bases dans le triangle de ref par rapport à x
    h=10**(-6)
    s1=fctbase(x+h,y,i,j,K)
    s2=fctbase(x+2*h,y,i,j,K)
    sm1=fctbase(x-h,y,i,j,K)
    sm2=fctbase(x-2*h,y,i,j,K)
    a=-1/12
    b=2/3
    s=(b*s1+a*s2-b*sm1-a*sm2)/h
    return(s)

def deriv_y_fctbase(x,y,i,j,K): ######## idem en y
    h=10**(-6)
    s1=fctbase(x,y+h,i,j,K)
    s2=fctbase(x,y+2*h,i,j,K)
    sm1=fctbase(x,y-h,i,j,K)
    sm2=fctbase(x,y-2*h,i,j,K)
    a=-1/12
    b=2/3
    s=(b*s1+a*s2-b*sm1-a*sm2)/h
    return(s)

def integraleww(i1,j1,i2,j2,K,rho,X): ############# calcul de l'intégrale de fonction de base i * fonction de base j, pour mettre dans M chapeau
    s=0
    for k1 in range(len(rho)):
        for k2 in range(len(rho)):
            w1=fctbase(X[k1],(1-X[k1])*X[k2],i1,j1,K)
            w2=fctbase(X[k1],(1-X[k1])*X[k2],i2,j2,K)
            s=s+rho[k1]*rho[k2]*w1*w2*(1-X[k1])
    return(s)


def integralexx(i1,j1,i2,j2,K,rho,X): ########## calcul des intégrales des dérivées (x,x et x,y et y,x et y,y) pour la matrice de rigidité
    s=0
    for k1 in range(len(rho)):
        for k2 in range(len(rho)):
            w1=deriv_x_fctbase(X[k1],(1-X[k1])*X[k2],i1,j1,K)
            w2=deriv_x_fctbase(X[k1],(1-X[k1])*X[k2],i2,j2,K)
            s=s+rho[k1]*rho[k2]*w1*w2*(1-X[k1])
    return(s)

def integralexy(i1,j1,i2,j2,K,rho,X):
    s=0
    for k1 in range(len(rho)):
        for k2 in range(len(rho)):
            w1=deriv_x_fctbase(X[k1],(1-X[k1])*X[k2],i1,j1,K)
            w2=deriv_y_fctbase(X[k1],(1-X[k1])*X[k2],i2,j2,K)
            s=s+rho[k1]*rho[k2]*w1*w2*(1-X[k1])
    return(s)

def integraleyx(i1,j1,i2,j2,K,rho,X):
    s=0
    for k1 in range(len(rho)):
        for k2 in range(len(rho)):
            w1=deriv_y_fctbase(X[k1],(1-X[k1])*X[k2],i1,j1,K)
            w2=deriv_x_fctbase(X[k1],(1-X[k1])*X[k2],i2,j2,K)
            s=s+rho[k1]*rho[k2]*w1*w2*(1-X[k1])
    return(s)

def integraleyy(i1,j1,i2,j2,K,rho,X):
    s=0
    for k1 in range(len(rho)):
        for k2 in range(len(rho)):
            w1=deriv_y_fctbase(X[k1],(1-X[k1])*X[k2],i1,j1,K)
            w2=deriv_y_fctbase(X[k1],(1-X[k1])*X[k2],i2,j2,K)
            s=s+rho[k1]*rho[k2]*w1*w2*(1-X[k1])
    return(s)

######################################################### CALCUL DES MATRICES DE MASSES ET DE RIGIDITÉ #################################################


Mc=np.zeros((ddloc,ddloc))######création matrice de masse locale
def masse_chapeau(K,loc2loc,rho,X):
    for i1 in range(K+1):
        for j1 in range(K-i1+1):
            i=loc2loc[i1,j1]
            for i2 in range(K+1):
                for j2 in range(K-i2+1):
                    j=loc2loc[i2,j2]
                    Mc[j,i]=integraleww(i1,j1,i2,j2,K,rho,X)
    return(Mc)

def masse(n2,K,Mc,loc2loc,loc2glob,nbGC,T):
    rowM=np.zeros(n2*ddloc*ddloc)
    colM=np.zeros(n2*ddloc*ddloc)
    dataM=np.zeros(n2*ddloc*ddloc) ############# matrice de masse globale en sparse
    i=0
    for n in range(n2):
        for i2 in range(K+1):
            for j2 in range(K-i2+1):
                jloc=loc2loc[i2,j2]
                jglob=loc2glob[n,i2,j2]
                for i1 in range(K+1):
                    for j1 in range(K-i1+1):
                        iloc=loc2loc[i1,j1]
                        iglob=loc2glob[n,i1,j1]
                        rowM[i]=jglob
                        colM[i]=iglob
                        dataM[i]=2*T[n]*Mc[jloc,iloc]
                        i=i+1
    Masse=sp.csc_matrix((dataM,(rowM,colM)),shape=(nbGC,nbGC))
    return(Masse)


def verif_masse(Masse,nbGC,Airepara): ######## fonction qui vérifie que 1.T*Masse*1= int 1= aire du parallélogramme
    un=np.ones((nbGC,1))
    v=np.dot(un.T,Masse*un)
    w=Airepara
    return(v,w)

def verif_masse2(Masse,nbGC,stilde,ptsGCtrié): ######### fonction qui vérifie que 1.T*Masse*(x-x0)=0 et (y-y0).T*Masse*1=0
    un=np.ones((nbGC,1))
    x0=stilde[0]*un
    y0=stilde[1]*un
    xx=np.reshape(ptsGCtrié[:,0],(nbGC,1))-x0
    yy=np.reshape(ptsGCtrié[:,1],(nbGC,1))-y0
    v=np.dot(xx.T,Masse*un)
    w=np.dot(un.T,Masse*yy)
    return(v,w)

#matrice M glob et loc sont vérifiées et bonnes à priori

Kcxx=np.zeros((ddloc,ddloc))
Kcyx=np.zeros((ddloc,ddloc))
Kcxy=np.zeros((ddloc,ddloc))
Kcyy=np.zeros((ddloc,ddloc))
def rig_locales(K,loc2loc,rho,X):########### création des rigidité locales
    for i1 in range(K+1):
        for j1 in range(K-i1+1):
            i=loc2loc[i1,j1]
            for i2 in range(K+1):
                for j2 in range(K-i2+1):
                    j=loc2loc[i2,j2]
                    Kcxx[j,i]=integralexx(i1,j1,i2,j2,K,rho,X)
                    Kcyx[j,i]=integraleyx(i1,j1,i2,j2,K,rho,X)
                    Kcxy[j,i]=integralexy(i1,j1,i2,j2,K,rho,X)
                    Kcyy[j,i]=integraleyy(i1,j1,i2,j2,K,rho,X)
    return(Kcxx,Kcxy,Kcyx,Kcyy)


Kloc=np.zeros((ddloc,ddloc))

def rigidité(n,pts,ele,T,Kcxx,Kcxy,Kcyx,Kcyy): ################## définition de la matrice de rigidité de l'élément n (les K chapeau sont définis plus bas)
    X1=pts[ele[n,0],:]
    X2=pts[ele[n,1],:]
    X3=pts[ele[n,2],:]
    J=np.zeros((2,2))
    J[0,0]=X2[0]-X1[0]
    J[0,1]=X3[0]-X1[0]
    J[1,0]=X2[1]-X1[1]
    J[1,1]=X3[1]-X1[1]
    B=np.dot(np.linalg.inv(J),np.linalg.inv(J).T)
    Kloc=2*T[n]*(B[0,0]*Kcxx+B[0,1]*Kcxy+B[1,0]*Kcyx+B[1,1]*Kcyy)
    return(Kloc)

def rig_globale(n2,K,pts,ele,T,Kcxx,Kcxy,Kcyx,Kcyy,loc2loc,loc2glob,nbGC):
    rowR=np.zeros(n2*ddloc*ddloc)
    colR=np.zeros(n2*ddloc*ddloc)
    dataR=np.zeros(n2*ddloc*ddloc) ######### matrice de rigidité globale en sparse
    i=0
    for n in range(n2):
        Kloc=rigidité(n,pts,ele,T,Kcxx,Kcxy,Kcyx,Kcyy)
        for i2 in range(K+1):
            for j2 in range(K-i2+1):
                jloc=loc2loc[i2,j2]
                jglob=loc2glob[n,i2,j2]
                for i1 in range(K+1):
                    for j1 in range(K-i1+1):
                        iloc=loc2loc[i1,j1]
                        iglob=loc2glob[n,i1,j1]
                        rowR[i]=jglob
                        colR[i]=iglob
                        dataR[i]=Kloc[jloc,iloc]
                        i=i+1
    R=sp.csc_matrix((dataR,(rowR,colR)),shape=(nbGC,nbGC))
    return(R)

def verif_rig(R,nbGC,ptsGCtrié,Airepara): ######### fonction qui vérifie que (ax+by+c).T*R*(a'x+b'y+c')=aire du parallélogramme*(aa'+bb')
    alpha1=np.random.uniform(0,10)
    alpha2=np.random.uniform(0,10)
    beta1=np.random.uniform(0,10)
    beta2=np.random.uniform(0,10)
    gamma1=np.random.uniform(0,10)
    gamma2=np.random.uniform(0,10)
    un=np.ones((nbGC,1))
    ptx=np.reshape(ptsGCtrié[:,0],(nbGC,1))
    pty=np.reshape(ptsGCtrié[:,1],(nbGC,1))
    xx=alpha1*un+beta1*ptx+gamma1*pty
    xxp=alpha2*un+beta2*ptx+gamma2*pty
    v=np.dot(xx.T,R*xxp)
    w=Airepara*(beta1*beta2+gamma1*gamma2)
    return(v,w)



########################################################## ASSEMBLAGE DE LA MATRICE DE BORD #############################################################


bas=0 ###### numérotation des cotés du parallélogramme
gauche=1
haut=2
droit=3

def normale(mg): # création des normales extérieures du parallélogramme
    nbas=np.zeros(2) 
    nbas[:]=(mg.M[0]*mg.M[1],-mg.alpha*(mg.alpha+1)-mg.M[0]**2)
    nbas=nbas/np.linalg.norm(nbas)
    ngauche=np.zeros(2)
    ngauche[:]=(-mg.alpha*(mg.alpha+1)-mg.M[1]**2,mg.M[0]*mg.M[1])
    ngauche=ngauche/np.linalg.norm(ngauche)
    return(nbas,ngauche)

def Z(x,typeface,stilde,nbas,ngauche,kappa): ########## fonction Z qu'on teste pour le bord : d.n
    d=np.zeros(2)
    d[0]=x[0]-stilde[0] ############ direction de l'onde (x-centre)
    d[1]=x[1]-stilde[1]
    d=d/np.linalg.norm(d)
    if typeface==bas:
        dn=np.dot(d,nbas)
    if typeface==gauche:
        dn=np.dot(d,ngauche)
    if typeface==haut:
        dn=np.dot(d,-nbas)
    if typeface==droit:
        dn=np.dot(d,-ngauche)
    return(dn)

def Z1(x,z,stilde,nbas,ngauche,kappa): ################## Z=1 pour tester que la mat de bord fait le perimetre
    return(1)

def ZX(x,y,stilde,nbas,ngauche,kappa): ################ Z=x pour tester que la matrice de bord fait x0*perimetre
    return(x[0])

def Z2(x,typeface,stilde,nbas,ngauche,kappa): ########## fonction Z qu'on teste pour le bord : d.n
    d=np.zeros(2)
    d[0]=x[0]-stilde[0] ############ direction de l'onde (x-centre)
    d[1]=x[1]-stilde[1]
    r=np.linalg.norm(d)
    d=d/np.linalg.norm(d)
    if typeface==bas:
        dn=np.dot(d,nbas)
    if typeface==gauche:
        dn=np.dot(d,ngauche)
    if typeface==haut:
        dn=np.dot(d,-nbas)
    if typeface==droit:
        dn=np.dot(d,-ngauche)
    dn=dn*1j*sc.special.hankel1(1,r*kappa)/sc.special.hankel1(0,r*kappa)
    return(dn)

Mcbord=np.zeros((K+1,K+1),dtype=complex)
I2=2*(K+1)
Xgauss2=np.zeros((I2,2))

def massebord_locale(nface,facebord,Z,rhoI2,XI2,ptsGDfaceDisc,stilde,nbas,ngauche,kappa): ########## création de la matrice de bord pour chaque face
    X1=np.zeros(2)
    X2=np.zeros(2)
    X1[:]=(facebord[nface,1],facebord[nface,2])
    X2[:]=(facebord[nface,3],facebord[nface,4])
    normf=np.linalg.norm(X2-X1)
    Xgauss2[:,0]=X1[0]+XI2*(X2[0]-X1[0])
    Xgauss2[:,1]=X1[1]+XI2*(X2[1]-X1[1])
    test=0
    if test==1:
        print('x1,x2',X1,X2)
        print('normf',normf)
        print('X',XI2)
        print('Xgauss',Xgauss2)
        print('rho',rhoI2)
        plt.figure()
        plt.title('pts de Gauss sur la face')
        plt.scatter(ptsGDfaceDisc[:,0],ptsGDfaceDisc[:,1])
        plt.scatter(Xgauss2[:,0],Xgauss2[:,1])
    for ilocf in range(K+1):
        for jlocf in range(K+1):
            s=0
            for m in range(I2):
                s=s+normf*rhoI2[m]*phi(XI2[m],ilocf,K)*phi(XI2[m],jlocf,K)*Z(Xgauss2[m,:],facebord[nface,5],stilde,nbas,ngauche,kappa)
                if test==1:
                    0
                    #print('Z',Z(Xgauss[m,:],facebord[nface,5]))
                    #print('type',facebord[nface,5])
            Mcbord[jlocf,ilocf]=s
    return(Mcbord)


def massebord(facebord,Z,nbfacebord,rhoI2,XI2,ptsGDfaceDisc,stilde,loc2globface,nbGC,nbas,ngauche,kappa): ############################# assemblage de la matrice de bord globale
    rowMbord=np.zeros(nbfacebord*(K+1)**2)
    colMbord=np.zeros(nbfacebord*(K+1)**2)
    dataMbord=np.zeros(nbfacebord*(K+1)**2,dtype=complex)
    i=0
    for nface in range(nbfacebord):
        Mcbord=massebord_locale(nface,facebord,Z,rhoI2,XI2,ptsGDfaceDisc,stilde,nbas,ngauche,kappa)
        for ilocf in range(K+1):
            for jlocf in range(K+1):
                iglob=loc2globface[nface,ilocf]
                jglob=loc2globface[nface,jlocf]
                rowMbord[i]=jglob
                colMbord[i]=iglob
                dataMbord[i]=Mcbord[jlocf,ilocf]
                i=i+1
    Mbord=sp.csc_matrix((dataMbord,(rowMbord,colMbord)),shape=(nbGC,nbGC))
    return(Mbord)

def verif_mbord_Z1(Mbord,nbGC,x1,x2,x3): #################### vérification que (Z=1) 1.T*Mbord*1=int_bord 1= perimètre
    un=np.ones((nbGC,1))
    v=np.dot(un.T,Mbord*un)
    a=np.sqrt((x1[0]-x2[0])**2+(x1[1]-x2[1])**2)
    b=np.sqrt((x2[0]-x3[0])**2+(x2[1]-x3[1])**2)
    perimetre=2*a+2*b
    return(v,perimetre)

def verif_mbord_ZX(Mbord,nbGC,x1,x2,x3,stilde): ####################### vérification que (Z=x) 1.T*Mbord*1=int_bord x =x0*perimètre
    un=np.ones((nbGC,1))
    v=np.dot(un.T,Mbord*un)
    a=np.sqrt((x1[0]-x2[0])**2+(x1[1]-x2[1])**2)
    b=np.sqrt((x2[0]-x3[0])**2+(x2[1]-x3[1])**2)
    perimetre=2*a+2*b
    return(v,stilde[0]*perimetre)

def integ(Z,facebord,nbfacebord,I,rho,X,stilde,nbas,ngauche,kappa): ############## fonction qui intègre Z sur le bord (Gauss sur chaque face)
    s=0
    for nface in range(nbfacebord):
        X1=np.zeros(2)
        X2=np.zeros(2)
        X1[:]=(facebord[nface,1],facebord[nface,2])
        X2[:]=(facebord[nface,3],facebord[nface,4])
        normf=np.linalg.norm(X2-X1)
        x=np.zeros((K+1,2))
        x[:,0]=facebord[nface,1]+X*(facebord[nface,3]-facebord[nface,1])
        x[:,1]=facebord[nface,2]+X*(facebord[nface,4]-facebord[nface,2])
        test=0
        if test==1:
            plt.figure()
            plt.scatter(facebord[nface,1],facebord[nface,2])
            print(facebord[nface,1],facebord[nface,2])
            plt.scatter(facebord[nface,3],facebord[nface,4])
            print(facebord[nface,3],facebord[nface,4])
            plt.scatter(x[:,0],x[:,1])
            plt.show()
        for m in range(I):
            s=s+rho[m]*Z(x[m,:],facebord[nface,5],stilde,nbas,ngauche,kappa)*normf
    return(s)

def verif_mbord(Mbord,nbfacebord,I,rho,X,stilde,nbGC,facebord,nbas,ngauche,Z,kappa): ######################### vérification que 1.T*Mbord*1=int_bord Z
    un=np.ones((nbGC,1))
    v=np.dot(un.T,Mbord*un)
    w=integ(Z,facebord,nbfacebord,I,rho,X,stilde,nbas,ngauche,kappa)
    return(v,w)

################################################# CRÉATION DU VECTEUR REPRÉSENTANT LE TERME SOURCE ######################################################


def nodal(nbGC,ptsGCtrié,f,stilde):
    F=np.zeros(nbGC)
    for i in range(nbGC): ########### numérotaion continue pour résoudre Au=F car matrice masse rigidité etc dans cette numérotation
        F[i]=f(ptsGCtrié[i,0],ptsGCtrié[i,1],stilde)
    return(F)

def traceF(ptsGCtrié,F):
    x = ptsGCtrié[:,0]
    y = ptsGCtrié[:,1]
    plt.figure()
    plt.tripcolor(x,y,F,cmap=cm.coolwarm) ################## plot en 2D
    plt.colorbar()
    plt.title('F partout')

def nodalpetit(ddlpetit,npetit,ddloc,ptsGCtrié,elepetit2grand,loc1toloc2,loc2glob,f,stilde):
    F2=np.zeros(ddlpetit)
    l=0
    for n in range(npetit): ########## numérotation du petit para pour pouvoir faire Uex bord=A F, A opérateur green --> Uex sur le bord
        for iloc in range(ddloc):
            i=loc2globpetit(n,iloc,elepetit2grand,loc1toloc2,loc2glob)
            F2[l]=f(ptsGCtrié[i,0],ptsGCtrié[i,1],stilde)
            l=l+1
    return(F2)

def traceFpetit(ptsGCtrié,npetit,ddloc,elepetit2grand,loc1toloc2,loc2glob,F2):
    x=np.zeros(np.size(F2))
    y=np.zeros(np.size(F2))
    l=0
    for n in range(npetit):
        for iloc in range(ddloc):
            iglob=loc2globpetit(n,iloc,elepetit2grand,loc1toloc2,loc2glob)
            x[l]=ptsGCtrié[iglob,0]
            y[l]=ptsGCtrié[iglob,1]
            l=l+1
    plt.figure()
    plt.tripcolor(x,y,F2,cmap=cm.coolwarm) ################## plot en 2D
    plt.colorbar()
    plt.title('F dans le petit parallélogramme')

def verifs(Masse,nbGC,Airepara,stilde,ptsGCtrié,R,Mbord,nbfacebord,I,rho,X,facebord,nbas,ngauche,kappa):
    v,w=verif_masse(Masse,nbGC,Airepara)
    print("masse",v,w)
    v,w=verif_masse2(Masse,nbGC,stilde,ptsGCtrié)
    print("masse0",v,w)
    v,w=verif_rig(R,nbGC,ptsGCtrié,Airepara)
    print("rigidité",v,w)
    v,w=verif_masse2(Mbord,nbGC,stilde,ptsGCtrié)
    print("rigité0",v,w)
    v,w=verif_mbord(Mbord,nbfacebord,I,rho,X,stilde,nbGC,facebord,nbas,ngauche,Z,kappa)
    print("mbord",v,w)
    #Mlocale=massebord_locale(3,facebord,Z)

def verif_Z(Z,nbfacebord,facebord,ptsGCtrié,loc2globface,x1,x2,x3,x4,stilde,nbas,ngauche,kappa):
    plt.figure() ############################### vérification que Z a la bonne forme
    plt.title('tracé des Z en fonction des faces')
    for nface in range(nbfacebord):
        if facebord[nface,5]==bas:
            for ilocf in range(K+1):
                dn=Z(ptsGCtrié[loc2globface[nface,ilocf],:],bas,stilde,nbas,ngauche,kappa)
                plt.scatter((ptsGCtrié[loc2globface[nface,ilocf],0]-x1[0])/(x2[0]-x1[0]),dn,color='r')
        if facebord[nface,5]==gauche:
            for ilocf in range(K+1):
                dn=Z(ptsGCtrié[loc2globface[nface,ilocf],:],gauche,stilde,nbas,ngauche,kappa)
                plt.scatter((ptsGCtrié[loc2globface[nface,ilocf],0]-x1[0])/(x4[0]-x1[0]),dn,color='b')
        if facebord[nface,5]==haut:
            for ilocf in range(K+1):
                dn=Z(ptsGCtrié[loc2globface[nface,ilocf],:],haut,stilde,nbas,ngauche,kappa)
                plt.scatter((ptsGCtrié[loc2globface[nface,ilocf],0]-x4[0])/(x3[0]-x4[0]),dn,color='g')
        if facebord[nface,5]==droit:
            for ilocf in range(K+1):
                dn=Z(ptsGCtrié[loc2globface[nface,ilocf],:],droit,stilde,nbas,ngauche,kappa)
                plt.scatter((ptsGCtrié[loc2globface[nface,ilocf],0]-x2[0])/(x3[0]-x2[0]),dn,color='y')
    

def verif_bord(facebord,Z1,ZX,nbfacebord,rhoI2,XI2,ptsGDfaceDisc,stilde,loc2globface,nbGC,x1,x2,x3,nbas,ngauche,kappa):
    Mbord2=massebord(facebord,Z1,nbfacebord,rhoI2,XI2,ptsGDfaceDisc,stilde,loc2globface,nbGC,nbas,ngauche,kappa)
    v,w=verif_mbord_Z1(Mbord2,nbGC,x1,x2,x3)
    print("perimetre",v,w)
    Mbord2=massebord(facebord,ZX,nbfacebord,rhoI2,XI2,ptsGDfaceDisc,stilde,loc2globface,nbGC,nbas,ngauche,kappa)
    v,w=verif_mbord_ZX(Mbord2,nbGC,x1,x2,x3,stilde)
    print("perim*x0",v,w)
