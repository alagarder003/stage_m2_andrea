import numpy as np
import matplotlib.pyplot as plt
import Maillage as m



############################################################## RÉCUPÉRATION DU MAILLAGE #################################################################


def crea_pts():
    file = open("maillage_reg.node", 'r') ################### récupération des noeud du maillage
    line=file.readline()
    a=line.split()
    n1=int(a[0]) ### nombre de noeuds du maillage
    pts=np.zeros((n1,2)) 
    for i in range(n1): ############ on rempli pts avec les points
        line=file.readline()
        a=line.split()
        pts[i,0]=float(a[1])
        pts[i,1]=float(a[2])
    file.close()
    return(n1,pts)


def crea_ele():
    file = open("maillage_reg.ele", 'r') ######### récupération des éléments
    line=file.readline()
    a=line.split()
    n2=int(a[0]) ###### nombre d'éléments
    ele=np.zeros((n2,3),dtype=int)
    for i in range(n2):
        line=file.readline()
        a=line.split()
        ele[i,0]=int(float(a[1]))-1
        ele[i,1]=int(float(a[2]))-1
        ele[i,2]=int(float(a[3]))-1
    #on fait -1 pour que les éléments soit numérotés par rapport à python donc on soustrait à la numérotation du programme triangle
    # ce qu'il y a dans les éléments ce sont les numéros des noeuds des triangles donc numéros des points définis juste avant
    file.close()
    return(n2,ele)


############################################################## DONNÉES ##################################################################################


K=4 ##### degré d'approximation (exemple:P2)
lambda1=20 ######## coef dans f



epsilon=10**(-8)

#n2 nb d'éléments
ddloc=int((K+1)*(K+2)*0.5) ######### nombre de degré de liberté locaux : ddl par éléments (triangle)

stilde=np.zeros(2) ############## source (centre) dans le parallélogramme

def source(mg):
    stilde[0]=mg.L*(mg.M[0]*mg.M[1]+mg.alpha*(mg.alpha+1)+mg.M[0]**2)/(2*mg.alpha*(mg.alpha+1))
    stilde[1]=mg.L*(mg.M[0]*mg.M[1]+mg.alpha*(mg.alpha+1)+mg.M[1]**2)/(2*mg.alpha*(mg.alpha+1))
    return(stilde)


def f2(r):
    return(np.exp(-lambda1*r**2))

def f(x,y,stilde): ################## terme source 
    r=np.linalg.norm([x-stilde[0],y-stilde[1]])
    return(f2(r))

def fonc_donnee(n1,n2):
    nbGD=n2*ddloc ######## nombre de degré de liberté globaux (en discontinu : face intérieures doublées)
    return(K,ddloc,nbGD,f,f2,lambda1)


################################### FONCTIONS FAITES AVEC LOIC CONCERNANT LE MAILLAGE #####################################################################

def Tk(X1,X2,X3,Xc): ################ transformation affine de l'élément de référence à un triangle quelconque
    x=np.zeros(2)
    x[0]=(X2[0]-X1[0])*Xc[0]+(X3[0]-X1[0])*Xc[1]+X1[0]
    x[1]=(X2[1]-X1[1])*Xc[0]+(X3[1]-X1[1])*Xc[1]+X1[1]
    return(x)

def Tkm1(X1,X2,X3,XX):
    A=np.zeros((2,2))
    A[0,0]=X2[0]-X1[0]
    A[0,1]=X3[0]-X1[0]
    A[1,0]=X2[1]-X1[1]
    A[1,1]=X3[1]-X1[1]
    b=np.zeros(2)
    b[0]=XX[0]-X1[0]
    b[1]=XX[1]-X1[1]
    Xc=np.linalg.solve(A,b)
    return(Xc)


loc2loc=np.zeros((K+1,K+1),dtype=int) #matrice du passage de loc 2 loc (matrice triang)
loc1toloc2=np.zeros((ddloc,2),dtype=int) #matrice qui donne iloc1 iloc2 à partir de iloc

def crea_loc2loc(K):
    l=0
    for i in range(K+1):
        for j in range(K+1-i):
            loc2loc[i,j]=l
            loc1toloc2[l,:]=(i,j)
            l=l+1
    return(loc2loc,loc1toloc2)

def loc2globfonc(n,i,j,K): ############ fonction qui associe un numéro global en numérotation galerkin discontinu (points répétés)
    nglob=(K+1)*(K+2)*0.5*n+loc2loc[i,j]
    return(int(nglob))

def coord(i,j,pts,ele):
    l=ele[i,j] #i numéro de l'élément; j de 0 à 2 numéro du point; renvoie les coords du sommet
    x=pts[l,0]
    y=pts[l,1]
    return(x,y)

def aire(n,pts,ele): ##################### calcul de l'aire de l'élément n
    X1=pts[ele[n,0],:]
    X2=pts[ele[n,1],:]
    X3=pts[ele[n,2],:]
    a=np.sqrt((X1[0]-X2[0])**2+(X1[1]-X2[1])**2)
    b=np.sqrt((X1[0]-X3[0])**2+(X1[1]-X3[1])**2)
    c=np.sqrt((X3[0]-X2[0])**2+(X3[1]-X2[1])**2)
    p=(a+b+c)*0.5
    A=np.sqrt(p*(p-a)*(p-b)*(p-c)) ############## formule d'heron
    return(A)

def aire_para(X1,X2,X3): ############## calcul de l'aire du parallélogramme grâce a trois points
    a=np.sqrt((X1[0]-X2[0])**2+(X1[1]-X2[1])**2)
    b=np.sqrt((X1[0]-X3[0])**2+(X1[1]-X3[1])**2)
    c=np.sqrt((X3[0]-X2[0])**2+(X3[1]-X2[1])**2)
    p=(a+b+c)*0.5
    A=np.sqrt(p*(p-a)*(p-b)*(p-c)) ############## formule d'heron
    return(2*A)

def taille_maillage(n2,pts,ele):
    H=0
    for n in range(n2): ####################### calcul h max (h longueur coté)
        X1=pts[ele[n,0],:]
        X2=pts[ele[n,1],:]
        X3=pts[ele[n,2],:]
        a=np.sqrt((X1[0]-X2[0])**2+(X1[1]-X2[1])**2)
        b=np.sqrt((X1[0]-X3[0])**2+(X1[1]-X3[1])**2)
        c=np.sqrt((X3[0]-X2[0])**2+(X3[1]-X2[1])**2)
        H=max(a,b,c,H)
    return(H)

def quel_element(x,y,pts,ele,n2,elem1,vec): ########################### fonction qui a un point quelconque associe l'élément dans lequel il se trouve
    epsilon=10**(-15)
    for n in range(2*n2-1):
        m=elem1+vec[n]
        if (0<=m)and(m<=n2-1):
            X1=pts[ele[m,0],:]
            X2=pts[ele[m,1],:]
            X3=pts[ele[m,2],:]
            i=0
            if ((X2[0]-x)*(X3[1]-y)-(X3[0]-x)*(X2[1]-y)+epsilon)>=0:
                i=i+1
                if ((X3[0]-x)*(X1[1]-y)-(X1[0]-x)*(X3[1]-y)+epsilon)>=0:
                    i=i+1
                    if ((X1[0]-x)*(X2[1]-y)-(X2[0]-x)*(X1[1]-y)+epsilon)>=0:
                        i=i+1
                        if i==3:
                            element=m
                            break
    return(element)


#################################### CRÉATION DES POINTS EN GALERKIN DISCONTINU PUIS EN CONTINU (FAIT AVEC LOIC) #########################################
    

XC=np.zeros((ddloc,2)) #matrice des ddloc sur l'ele de ref

def Xchapeau(K,loc2loc):
    for i in range(K+1):
        for j in range(K+1-i):
            k=loc2loc[i,j]
            XC[k,0]=i/K
            XC[k,1]=j/K
    return(XC)


def crea_ptsGD(K,n2,pts,ele,XC,nbGD):
    ptsGD=np.zeros((nbGD,3)) #ddglob GD (répété non trié)
    for n in range(n2):
        X1=pts[ele[n,0],:]
        X2=pts[ele[n,1],:]
        X3=pts[ele[n,2],:]
        for i in range(K+1):
            for j in range(K+1-i):
                Xc=XC[(loc2loc[i,j]),:]
                ptsGD[loc2globfonc(n,i,j,K)][0]=Tk(X1,X2,X3,Xc)[0]
                ptsGD[loc2globfonc(n,i,j,K)][1]=Tk(X1,X2,X3,Xc)[1]
                ptsGD[loc2globfonc(n,i,j,K)][2]=loc2globfonc(n,i,j,K)
    return(ptsGD)



def crea_glob2loc(n2,K,nbGD):
    glob2loc=np.zeros((nbGD,3),dtype=int) # création da la matrice glob2loc contenant élément + iloc1 + iloc2
    l=0
    for n in range(n2):
        for i in range(K+1):
            for j in range(K+1-i):
                glob2loc[l,:]=[n,i,j]
                l=l+1
    return(glob2loc)


def crea_loc2glob(nbGD,ptsGDtrié,tri,glob2loc,n2):
    loc2glob=np.zeros((n2,K+1,K+1),dtype=int) # matrice 3D qui à un numéro d'élé + iloc1 + iloc2 associe un numéro iglob (galerkin continu:trié effacé)
    for i1 in range(nbGD):
        i2=int(ptsGDtrié[i1,2])
        (n,i,j)=glob2loc[i2,:]
        iglob=tri[i1]
        loc2glob[n,i,j]=iglob
    return(loc2glob)


def crea_aire(n2,pts,ele):
    T=np.zeros(n2) # aire de chaque élément 
    for n in range(n2):
        T[n]=aire(n,pts,ele)
    return(T)

################################################ TOUTES LES FONCTIONS CONCERNANT LE BORD DU MAILLAGE #####################################################



bas=0 ###### numérotation des cotés du parallélogramme
gauche=1
haut=2
droit=3


def test_bord(x,mg): ################ fonction pour savoir si on appartient au bord du domaine
    i=-1
    yb=x[0]*mg.M[0]*mg.M[1]/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))
    yd=x[0]*(mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])-mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])
    yh=x[0]*mg.M[0]*mg.M[1]/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))+mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))
    yg=x[0]*(mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])
    if abs(x[1]-yb)<epsilon:
        i=0
    if abs(x[1]-yg)<epsilon:
        i=1
    if abs(x[1]-yh)<epsilon:
        i=2
    if abs(x[1]-yd)<epsilon:
        i=3
    return(i)



 ######### création d'un matrice contenant: le numéro global discontinue non effacé de la face, les extrémités
#le milieu de la face, sa nature, l'élément associé, et le numéro de face(0:2)
def crea_face(n2,pts,ele,nbface):
    face=np.zeros((nbface,10))
    i=0
    for n in range(n2): 
        X1=pts[ele[n,0],:]
        X2=pts[ele[n,1],:]
        X3=pts[ele[n,2],:]
        face[i,:]=(i,X1[0],X1[1],X2[0],X2[1],(X1[0]+X2[0])/2,(X1[1]+X2[1])/2,0,n,0)
        face[i+1,:]=(i+1,X2[0],X2[1],X3[0],X3[1],(X2[0]+X3[0])/2,(X2[1]+X3[1])/2,0,n,1)
        face[i+2,:]=(i+2,X1[0],X1[1],X3[0],X3[1],(X3[0]+X1[0])/2,(X3[1]+X1[1])/2,0,n,2)
        i=i+3
    return(face)

def quelbord(face,nbface,mg):
    for nface in range(nbface): ############## dans la colonne 7 de face : -1 face intérieure, 0,1,2,3 = face du bord
        x=np.zeros(2)
        x[0]=face[nface,5]
        x[1]=face[nface,6]
        face[nface,7]=test_bord(x,mg)
    return(face)

def calcul_nbfacebord(face,nbface): ########### calcul du normbre de face de bord
    l=0
    for nface in range(nbface): ###### calcul du nombre de faces extérieures
        if face[nface,7]!=-1:
            l=l+1
    nbfacebord=l 
    return(nbfacebord)
    
def crea_facebord(nbface,face,nbfacebord):
    facebord=np.zeros((nbfacebord,8)) ############# matrice contenant quasiment la même chose que "face" mais avec uniquement les faces extérieures
    ######### j'ai enlevé le point du milieu (inutile mtn) 
    l=0
    for nface in range(nbface):
        if face[nface,7]!=-1:
            facebord[l,0]=l
            facebord[l,1:5]=face[nface,1:5]
            facebord[l,5:8]=face[nface,7:10]
            l=l+1
    return(facebord)


def crea_ptsGDfaceDisc(K,nbfacebord,facebord,ddlfacebord,loc2glob):
    ptsGDfaceDisc=np.zeros((ddlfacebord,3)) ######## création des ddl sur les faces (extérieures uniquement) + son numéro GC
    l=0
    for nface in range(nbfacebord):
        for ilocf in range(K+1):
            ptsGDfaceDisc[l,0]=facebord[nface,1]+ilocf*(facebord[nface,3]-facebord[nface,1])/K
            ptsGDfaceDisc[l,1]=facebord[nface,2]+ilocf*(facebord[nface,4]-facebord[nface,2])/K
            if facebord[nface,7]==0:
                ptsGDfaceDisc[l,2]=loc2glob[int(facebord[nface,6]),ilocf,0]
            elif facebord[nface,7]==1:
                ptsGDfaceDisc[l,2]=loc2glob[int(facebord[nface,6]),K-ilocf,ilocf]
            else:
                ptsGDfaceDisc[l,2]=loc2glob[int(facebord[nface,6]),0,ilocf]
            l=l+1
    return(ptsGDfaceDisc)


def crea_loc2globface(K,nbfacebord,ptsGDfaceDisc):
    loc2globface=np.zeros((nbfacebord,K+1),dtype=int) ########### matrice qui à un numéro de face + ilocf associe le numéro iglob (le même que loc2glob)
    l=0
    for nface in range(nbfacebord):
        for ilocf in range(K+1):
            loc2globface[nface,ilocf]=ptsGDfaceDisc[l,2]
            l=l+1
    return(loc2globface)

############################################################# PETIT PARALLÉLOGRAMME #####################################################################


def test_petitpara(x,mg): ################ fonction pour savoir si on appartient au petit parallélogramme
    i=0
    yb=x[0]*mg.M[0]*mg.M[1]/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))+0.45*mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))
    yd=x[0]*(mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])-0.55*mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])
    yh=x[0]*mg.M[0]*mg.M[1]/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))+0.55*mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]**2+ mg.alpha*(mg.alpha+1))
    yg=x[0]*(mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])-0.45*mg.L*(mg.M[0]**2+mg.M[1]**2+ mg.alpha*(mg.alpha+1))/(mg.M[0]*mg.M[1])
    if (x[1]-yb)>0 and (x[1]-yd)>0 and (x[1]-yh)<0 and (x[1]-yg)<0:
        i=1
    return(i)



def crea_centre(n2,pts,ele):
    centre=np.zeros((n2,2)) ########### calcul du centre de chaque éléments, pour voir s'il est dans le petit parallélogramme
    for n in range(n2):
        X1=pts[ele[n,0],:]
        X2=pts[ele[n,1],:]
        X3=pts[ele[n,2],:]
        centre[n,:]=(X1+X2+X3)/3
    return(centre)

def calcul_npetit(centre,n2,mg):
    l=0
    for n in range(n2):
        if test_petitpara(centre[n,:],mg)==1:
            l=l+1
    return(l)


def crea_ptsDGpetit(n2,ddloc,ptsGCtrié,npetit,ddlpetit,loc1toloc2,loc2glob,mg,centre):
    elepetit2grand=np.zeros(npetit,dtype=int) ######### table qui a un numéro d'élément dans le petit para lui associe son numéro dans le grand para
    ptsGDpetit=np.zeros((ddlpetit,3)) ######### tableau des points (ddl disc) dans le petit para + numéro final continu associé
    glob2locpetit=np.zeros((ddlpetit,2),dtype=int) ######### associe au numéro de point (numero disc dans le petit para) un numéro d'élé (petit para) + iloc
    l=0
    l2=0
    for n in range(n2):
        if test_petitpara(centre[n,:],mg)==1:
            elepetit2grand[l]=n
            for iloc in range(ddloc):
                (i1,i2)=loc1toloc2[iloc]
                iglob=loc2glob[n,i1,i2]
                ptsGDpetit[l2,:]=[ptsGCtrié[iglob,0],ptsGCtrié[iglob,1],iglob]
                glob2locpetit[l2,:]=[l,iloc]
                l2=l2+1
            l=l+1
    return(elepetit2grand,ptsGDpetit,glob2locpetit)


def loc2globpetit(npetit,iloc,elepetit2grand,loc1toloc2,loc2glob): ########### fonction qui au numéro d'élément (dans le petit para) + iloc associe son numéro global final continu (grand para)
    n=elepetit2grand[npetit]
    (i1,i2)=loc1toloc2[iloc]
    iglob=loc2glob[n,i1,i2]
    return(iglob)


def trace_bord(nbfacebord,facebord,ptsGCtrié,loc2globface):
    plt.figure() ####################### vérification que les faces sont bien associée au bon côté
    plt.title('tracé des bords du parallélograme (+petit?)')
    for nface in range(nbfacebord):
        if facebord[nface,5]==bas:
            for ilocf in range(K+1):
                plt.scatter(ptsGCtrié[loc2globface[nface,ilocf],0],ptsGCtrié[loc2globface[nface,ilocf],1],color='r')
        if facebord[nface,5]==gauche:
            for ilocf in range(K+1):
                plt.scatter(ptsGCtrié[loc2globface[nface,ilocf],0],ptsGCtrié[loc2globface[nface,ilocf],1],color='b')
        if facebord[nface,5]==haut:
            for ilocf in range(K+1):
                plt.scatter(ptsGCtrié[loc2globface[nface,ilocf],0],ptsGCtrié[loc2globface[nface,ilocf],1],color='g')
        if facebord[nface,5]==droit:
            for ilocf in range(K+1):
                plt.scatter(ptsGCtrié[loc2globface[nface,ilocf],0],ptsGCtrié[loc2globface[nface,ilocf],1],color='y')

def trace_petit(ptsGDpetit):
    plt.scatter(ptsGDpetit[:,0],ptsGDpetit[:,1])

from bibmatrice import fctbase

def eval_uh(x,y,U,pts,ele,n2,loc1toloc2,loc2glob,K,elem1,vec):
    element=quel_element(x,y,pts,ele,n2,elem1,vec)
    X1=pts[ele[element,0],:]
    X2=pts[ele[element,1],:]
    X3=pts[ele[element,2],:]
    XX=np.zeros(2)
    XX[0]=x
    XX[1]=y
    s=0
    for iloc in range(ddloc):
        (i,j)=loc1toloc2[iloc,:]
        iglob=loc2glob[element,i,j]
        Xt=Tkm1(X1,X2,X3,XX)
        s=s+U[iglob]*fctbase(Xt[0],Xt[1],i,j,K)
    return(s,element)