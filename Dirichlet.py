import numpy as np
import scipy.sparse as sp


def dirichlet(E,nbfacebord,loc2globface,nbGC,K): ############## fonction qui met des 0 sur les lignes de bord
    rowE,colE=sp.csc_matrix.nonzero(E)
    for nface in range(nbfacebord):
        for iloc in range(K+1):
            iglob=loc2globface[nface,iloc]
            row=np.argwhere(rowE==iglob)
            for i in range(np.shape(row)[0]):
                E.data[row[i]]=0
    E=sp.csc_matrix((E.data,(rowE,colE)),shape=(nbGC,nbGC))
    return(E)

def Id_dirichlet(ddlfacebord,nbfacebord,K,loc2globface,nbGC): ############# fonction qui met des 1 sur les diag des lignes qui concernent le bord
    rowI=np.zeros(ddlfacebord)
    dataI=np.zeros(ddlfacebord)
    l=0
    for nface in range(nbfacebord):
        for iloc in range(K+1):
            iglob=loc2globface[nface,iloc]
            rowI[l]=iglob
            dataI[l]=1
            l=l+1
    II=sp.csc_matrix((dataI,(rowI,rowI)),shape=(nbGC,nbGC))
    return(II)

def Fdirichlet(MF,nbfacebord,loc2globface,K): ###### fonction qui met des 0 sur le bord dans le terme source 
    l=0
    for nface in range(nbfacebord):
        for iloc in range(K+1):
            iglob=loc2globface[nface,iloc]
            MF[iglob]=0
            l=l+1
    return(MF)

def Ud_dirichlet(Uexbord,nbGC,nbfacebord,loc2globface,K): ###### fonction qui met les valeur que doit valloir U sur les lignes qui concernent le bord
    Ud=np.zeros(nbGC,dtype=complex)
    l=0
    for nface in range(nbfacebord):
        for iloc in range(K+1):
            iglob=loc2globface[nface,iloc]
            Ud[iglob]=Uexbord[l]
            l=l+1
    return(Ud)
